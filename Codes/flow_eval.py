import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import os
import scipy.misc
import numpy as np
import scipy.io as scio
from tensorflow.python.tools import inspect_checkpoint as ckpt
from models import generator, discriminator, flownet, initialize_flownet, sfm_pose, initialize_sfm
from loss_functions import intensity_loss, gradient_loss, motion_loss
from utils import DataLoader, load, save, psnr_error, np_load_frame
from constant import const

flow_height, flow_width = const.FLOW_HEIGHT, const.FLOW_WIDTH
model_name = 'model.ckpt-80000'
model_dir = 'checkpoints'
# none
#model_para = 'EgoSeg_l_2_alpha_1_lp_0.1_adv_1.0_gdl_0.1_flow_1.0_motionnewe_1.0'
#model_para = 'EgoSeg_l_2_alpha_1_lp_0.1_adv_1.0_gdl_0.1_flow_1.0_motion_0.0'
save_dir = 'evaluation'
img_name = 'flow_1.0_motion_0.0_d'

pr_path = os.path.join(save_dir,"41003.jpg")
pr_img = np_load_frame(pr_path, const.HEIGHT, const.WIDTH)
pr_frame = tf.expand_dims(tf.convert_to_tensor(pr_img),0)

gt_path = os.path.join(save_dir,"41004.jpg")
gt_img = np_load_frame(gt_path, const.HEIGHT, const.WIDTH)
gt_frame = tf.expand_dims(tf.convert_to_tensor(gt_img),0)

gen_path = os.path.join(save_dir,img_name+'.jpg')
gen_img = np_load_frame(gen_path, const.HEIGHT, const.WIDTH)
gen_frame = tf.expand_dims(tf.convert_to_tensor(gen_img),0)

test_gt_flow = flownet(input_a=pr_frame, input_b=gt_frame,
                            height=flow_height, width=flow_width, reuse=True)
test_pred_flow = flownet(input_a=pr_frame, input_b=gen_frame,
                              height=flow_height, width=flow_width, reuse=True)
                       
with tf.Session() as sess:
    
    # initialize flownet
    initialize_flownet(sess, const.FLOWNET_CHECKPOINT)
    # initialize sfm
    #initialize_sfm(sess, const.SFM_CHECKPOINT)
        
    #saver = tf.train.Saver(var_list=tf.global_variables(), max_to_keep=None)
    #saver.restore(sess, os.path.join(model_dir, model_para, model_name))
    print("Model successfully restored.")
    
    print(test_gt_flow)
    np.save(os.path.join(save_dir,'gt_d.npy'), sess.run(test_gt_flow[0]))
    #test_gt_flow = test_gt_flow-tf.reduce_min(test_gt_flow)
    #test_gt_flow = test_gt_flow/tf.reduce_max(test_gt_flow)*255
    #flow_gt = sess.run(test_gt_flow[0,:,:,0])
    #scipy.misc.imsave(os.path.join(save_dir, img_name+'fwgt.jpg'), flow_gt)
    
    #np.save(os.path.join(save_dir,img_name+'pr.npy'), sess.run(test_pred_flow[0]))
    #test_pred_flow = test_pred_flow-tf.reduce_min(test_pred_flow)
    #test_pred_flow = test_pred_flow/tf.reduce_max(test_pred_flow)*255
    #flow_pred = sess.run(test_pred_flow[0,:,:,0])
    #scipy.misc.imsave(os.path.join(save_dir, img_name+'fwpr.jpg'), flow_pred)


                       