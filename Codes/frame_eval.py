import tensorflow as tf
import os
import scipy.misc
import numpy as np
import scipy.io as scio
from tensorflow.python.tools import inspect_checkpoint as ckpt
from models import generator, discriminator, flownet, initialize_flownet, sfm_pose, initialize_sfm
from loss_functions import intensity_loss, gradient_loss, motion_loss
from utils import DataLoader, load, save, psnr_error, np_load_frame
from constant import const

flow_height, flow_width = const.FLOW_HEIGHT, const.FLOW_WIDTH
model_name = 'model.ckpt-80000'
model_dir = 'checkpoints'
# none
#model_para = 'EgoSeg_l_2_alpha_1_lp_0.1_adv_1.0_gdl_0.1_flow_1.0_motionnew_1.0'
model_para = 'EgoSeg_l_2_alpha_1_lp_0.1_adv_1.0_gdl_0.1_flow_0.0_motion_1.0'
save_dir = 'evaluation'
num_pred = 3
num_his = const.NUM_HIS
height, width = 1024, 436

# load a sequence of 5 frames
test_dir = os.path.join(const.TEST_FOLDER,"89")
seq_name = "bamboo_1"

# 0-22
# 23-44
for offset in range(22,23):
    for n in range(offset,offset+num_his+num_pred):
    # table
    #img_file = os.path.join(const.TEST_FOLDER,"53","2201"+str(n)+".jpg")
    # door
    #img_file = os.path.join(const.TEST_FOLDER,"37","3810"+str(n)+".jpg")
    # gt sintel
        if n<9:
            img_file = os.path.join(save_dir,seq_name,"frame_000"+str(n+1)+".png")
        else:
            img_file = os.path.join(save_dir,seq_name,"frame_00"+str(n+1)+".png")
        print(img_file)
        curr_img = np_load_frame(img_file, height, width)
        curr_img = tf.convert_to_tensor(curr_img)
        if n == offset:
            image_seq = curr_img
        else:
            image_seq = tf.concat([image_seq, curr_img],2) 
        
    image_seq = image_seq[None,...]
    test_inputs = image_seq[...,0:3*num_his]
    test_gt = image_seq[...,-num_pred*3:-num_pred*3+3]


#with tf.variable_scope('generator', reuse=None):
#    print('testing = {}'.format(tf.get_variable_scope().name))
    
#    test_outputs = generator(test_inputs, layers=4, output_channel=3)
    
#    test_psnr_error = psnr_error(gen_frames=test_outputs, gt_frames=test_gt)

    
    gt_seq = image_seq[...,-(num_pred*2-1)*3:]
#pred_seq = tf.concat([image_seq[..., (num_his-num_pred+1)*3:num_his*3], test_outputs, image_seq[..., -(num_pred-1)*3:]],3) 
#pred_seq = tf.concat([test_inputs, test_outputs],3)
    gt_mat = sfm_pose(gt_seq, height, width, num_pred)
    
    if offset == 22:
        gt_trmat = gt_mat[None,...]
    else:
        gt_trmat = tf.concat([gt_trmat, gt_mat[None,...]], 0)
    print(gt_trmat)
#pred_trmat = sfm_pose(pred_seq, height, width, num_pred)

#test_gt_flow = flownet(input_a=test_inputs[..., -3:], input_b=test_gt,height=flow_height, width=flow_width, reuse=None)
#test_pred_flow = flownet(input_a=test_inputs[..., -3:], input_b=test_outputs,height=flow_height, width=flow_width, reuse=True)


with tf.Session() as sess:
    # Restore variables from disk.
    
    # initialize flownet
    #initialize_flownet(sess, const.FLOWNET_CHECKPOINT)
    # initialize sfm
    initialize_sfm(sess, const.SFM_CHECKPOINT)
        
    #saver = tf.train.Saver(var_list=tf.global_variables(), max_to_keep=None)
    #saver.restore(sess, os.path.join(model_dir, model_para, model_name))
    print("Model successfully restored.")
    # remap the image from [-1,1] to [0,256]
    #result = sess.run((test_outputs[0]+1.0)*127.5)
    # save image to 
    #scipy.misc.imsave(os.path.join(save_dir,model_para+'.'+model_name+'.jpg'), result)
    #print('model:',model_para)
    #print('psnr=',sess.run(test_psnr_error))
    for offset in range(22,23):
        gt=sess.run(gt_trmat[offset-22])
    #pr=sess.run(pred_trmat)
        file_path = os.path.join(save_dir,seq_name+'_motion',seq_name+'_'+str(offset+num_his+1)+'.npy')
        print('saving... ',file_path)
        np.save(file_path,gt)
    #np.save(os.path.join(save_dir,'alley_1_pr.npy'),pr)

    
