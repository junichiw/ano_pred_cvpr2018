import tensorflow as tf
import numpy as np


def flow_loss(gen_flows, gt_flows):
    return tf.reduce_mean(tf.abs(gen_flows['flow'] - gt_flows['flow']))


def intensity_loss(gen_frames, gt_frames, l_num):
    """
    Calculates the sum of lp losses between the predicted and ground truth frames.

    @param gen_frames: The predicted frames at each scale.
    @param gt_frames: The ground truth frames at each scale
    @param l_num: 1 or 2 for l1 and l2 loss, respectively).

    @return: The lp loss.
    """
    # the origin of error nan
    in_loss = tf.reduce_mean(tf.abs(gen_frames - gt_frames) ** l_num)
    #in_loss = tf.reduce_mean(tf.abs((gen_frames - gt_frames)*(gen_frames - gt_frames)))
    
    return in_loss


def gradient_loss(gen_frames, gt_frames, alpha):
    """
    Calculates the sum of GDL losses between the predicted and ground truth frames.

    @param gen_frames: The predicted frames at each scale.
    @param gt_frames: The ground truth frames at each scale
    @param alpha: The power to which each gradient term is raised.

    @return: The GDL loss.
    """
    # calculate the loss for each scale
    # create filters [-1, 1] and [[1],[-1]] for diffing to the left and down respectively.

    channels = gen_frames.get_shape().as_list()[-1]
    pos = tf.constant(np.identity(channels), dtype=tf.float32)     # 3 x 3
    neg = -1 * pos
    filter_x = tf.expand_dims(tf.stack([neg, pos]), 0)  # [-1, 1]
    filter_y = tf.stack([tf.expand_dims(pos, 0), tf.expand_dims(neg, 0)])  # [[1],[-1]]
    strides = [1, 1, 1, 1]  # stride of (1, 1)
    padding = 'SAME'

    gen_dx = tf.abs(tf.nn.conv2d(gen_frames, filter_x, strides, padding=padding))
    gen_dy = tf.abs(tf.nn.conv2d(gen_frames, filter_y, strides, padding=padding))
    gt_dx = tf.abs(tf.nn.conv2d(gt_frames, filter_x, strides, padding=padding))
    gt_dy = tf.abs(tf.nn.conv2d(gt_frames, filter_y, strides, padding=padding))

    grad_diff_x = tf.abs(gt_dx - gen_dx)
    grad_diff_y = tf.abs(gt_dy - gen_dy)

    # condense into one tensor and avg
    return tf.reduce_mean(grad_diff_x ** alpha + grad_diff_y ** alpha)
    #return tf.reduce_mean(grad_diff_x + grad_diff_y)

def motion_loss(gen_transmat, gt_transmat):
    """
    Calculates the sum of motion loss between the predicted anf ground truth frames.
    
    @param gen_transmat: The predicted transformation matrix at each scale [4x7]
    @param gt_transmat: the ground truth transformation matrix at each scale [4x7]
    
    @return: The motion loss.
    """ 
    
    # the loss of translation, directly calculating with distance, i.e MSE loss
    translation_loss = tf.reduce_mean(tf.sqrt(tf.reduce_sum(tf.square(gen_transmat[...,:3]-gt_transmat[...,:3]),2)))
    # scale to unit quaternions
    #scale_gen = tf.sqrt(tf.reduce_sum(tf.square(gen_transmat[...,3:]),2))
    #scale_gt = tf.sqrt(tf.reduce_sum(tf.square(gt_transmat[...,3:]),2))
    dot_prd = tf.reduce_sum((gen_transmat[...,3:])*(gt_transmat[...,3:]),2)
    # using (1-cosθ)/2=1-<q1,q2>^2 to represent the value of θ beacause of its range of [0,1]
    # rotation_loss = tf.reduce_mean(1.0 - tf.square(dot_prd))
    # using θ as the value 𝜃=arccos(<𝑞1,𝑞2>)
    rotation_loss = tf.reduce_mean(tf.acos(dot_prd)/np.pi)
    #
    #w_trans = 0.5
    #w_rot = 0.5
    mt_loss = translation_loss + rotation_loss
    return mt_loss

